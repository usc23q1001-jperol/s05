# class SampleClass(object):
# 	def __init__(self, year):
# 		self.year = year

# 	def show_year(self):
# 		print(f'The year is: {self.year}')

# myObj = SampleClass(2020)

# print(myObj.year)
# myObj.show_year()


# # Inheritance
# # Polymorphism
# # Abstraction

# # [SECTION] Encapsulation
# # Encapsulation is a mechanism of wrapping the attributes
# # and codes acting on the methods together as a single unit
# # "data hiding"

# # The prefix underscore(_) is used as a warning for
# # developers that means:
# # "Please be careful about this attribute or method,
# # dont use it outside the declared Class."
# class Person():
# 	def __init__(self):
# 		self._name = "John Doe"

# 	# setter method
# 	def set_name(self, name):
# 		self._name = name

# 	def get_name(self):
# 		print(f'Name of Person: {self._name}')

# p1 = Person()
# p1.get_name()
# p1.set_name("Jane Smith")
# p1.get_name()

# # Mini exercise 1
# class Person():
# 	def __init__(self):
# 		self.name = "John Doe"
# 		self._age = 30

# 	def get_age(self):
# 		print(f'Age of Person:{self._age}')

# 	def set_age(self, age):
# 		self._age = age

# pipi = Person()

# pipi.get_age()
# pipi.set_age(40)
# pipi.get_age()

# # Mini exercise 2
# class Student(Person):
# 	def __init__(self, student_no, course, year_level):
# 		super().__init__()
# 		self._student_no = student_no
# 		self._course = course
# 		self._year_level = year_level
# 		print(f"Student number of Student is {self._student_no}")

# 	def get_course(self):
# 		print(f"Course of Student is {self._course}")

# 	def get_year_level(self):
# 		print(f"The year level of Student is {self._year_level}")

# 		# setters
# 		def set_student_no(self, student_no):
# 			self._student_no = student_no

# 		def set_course(self, course):
# 			self._course = course

# 		def set_year_level(self, year_level):
# 			self._year_level = year_level


# #Inheritance

# class Employee(Person):

# 	def __init__(self, employeeId):
# 		super().__init__()
# 		self._employeeId = employeeId

# 	def get_employeeId(self):
# 		printf(f'The Employee ID is {self._employeeId}')

# 	def set_employeeId(self, employeeId):
# 		self._employeeId = employeeId

# 	def get_details(self):
# 		print(f"{self._employeeId} belongs to {self._name}")

# emp1 = Employee("Emp-001")
# emp1.get_details()


# class Student(Person):
# 	def __init__(self, stundentNumber, course, yearLevel):
# 		super().__init__()
# 		self._studentNumber = stundentNumber
# 		self._course = course
# 		self._yearLevel= yearLevel

# 	def get_studentNumber(self):
# 		print(f'Student Number of Person:{self._studentNumber}')

# 	def set_studentNumber(self, stundentNumber):
# 		self._studentNumber = stundentNumber

# 	def get_course(self):
# 		print(f'Course of Person:{self._course}')

# 	def set_course(self, course):
# 		self._course = course

# 	def get_yearLevel(self):
# 		print(f'Year level of Person:{self._yearLevel}')

# 	def set_yearLevel(self, yearLevel):
# 		self._yearLevel = yearLevel

# 	def get_details(self):
# 		print(f"{self._name} is currently in year {self._yearLevel} taking up {self._course}")

# stud = Student("stdt-001", "BSCS" , 4)
# stud.get_details()

# class Admin():
# 	def is_admin(self):
# 		print(True)

# 	def user_type(self):
# 		print('Admin User')

# class Customer():
# 	def is_admin(self):
# 		print(False)

# 	def user_type(self):
# 		print("Customer User")

# # Define a test function taht will take an object called obj.

# def test_function(obj):
# 	obj.is_admin()
# 	obj.user_type()

# # Create object instance of Admin and Customer
# user_admin = Admin()
# user_customer = Customer()

# # Pass the created instance to the test function.
# test_function(user_admin)
# test_function(user_customer)

# # Polymorphism with Class Methods
# # Python uses two different lass types in the same way.

# class TeamLead():
# 	def occupation(self):
# 		print('Team Lead')

# 	def hasAuth(self):
# 		print(True)

# class TeamMember():
# 	def occupation(self):
# 		print('Team Member')

# 	def hasAuth(self):
# 		print(False)

# tl1 = TeamLead()
# tm1 = TeamMember()

# for person in (tl1, tm1):
# 	person.occupation()

# Polymorphism with Inheritance
# Polymorphism in python defines methods in hild calss
# that have the same name as methods in the aprent class
# "Method Overriding"

class Zuitt():
	def tracks(self):
		print("We are currently offering 3 tracks")

	def num_of_hours(self):
		print("Learn web dev in3 60 hours!")

class DeveloperCareer(Zuitt):
	def num_of_hours(self):
		print("Learn the basics of web dev in 240 hours!")

class PiShapedCareer(Zuitt):
	def num_of_hours(self):
		print("Learn skills for no-code in 140 hours!")

class PiShapedCareer(Zuitt):
	def num_of_hours(self):
		print("Learn skills for no-code in 140 hours!")

class ShortCourses(Zuitt):
	def num_of_hours(self):
		print("Learn advanced topics in web dev in 20 hours!")

course1 = DeveloperCareer()
course2 = DeveloperCareer()
course3 = DeveloperCareer()

course1.num_of_hours()
course2.num_of_hours()
course3.num_of_hours()

# [SECTION] Abstraction
# An abstact class can be considered as a blueprint for other class.

# Abstract Base Classes (abc)
# The import tells the program to get the abc module of python to be used.
from abc import ABC, abstractclassmethod

# The class Polygon inherits the abstract class module.
class Polygon(ABC):
	# Created an abstract method called print_number_of_sides
	# that needs
	@abstractclassmethod
	def print_number_of_sides(self):
		# This denotes that the method doesnt do anything
		pass

class Triangle(Polygon):
	def __init__(self):
		super().__init__()

	# Since Triangle class inherited the Polygon class
	# It must now implement the abstract method
	def print_number_of_sides(self):
		print("This polygon has 3 sides")

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print("This polygon has 5 sides")
		
shape1 = Triangle()
shape2 = Pentagon()

shape1.print_number_of_sides()
shape2.print_number_of_sides()